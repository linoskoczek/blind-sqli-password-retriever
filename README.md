# Blind SQLi Password Retriever

Blind SQL Injection example which retrieves password of an administrator. 
This actual code was used to solve [Blind SQL injection with conditional errors on Portswigger Academy](https://portswigger.net/web-security/sql-injection/blind)